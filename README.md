# PulseBall

## Pulseball Rankings Table

The primary goal is to produce a web application that implements a rankings predictor for the fictitious game of Pulseball.

Exercise should be completed entirely in JavaScript and run without a back-end.

## Task

Build a rankings predictor web application in JavaScript that gets initialised through a function PULSEBALL.init( rankingsJson ), where rankingsJson is in the format of the rankings table JSON examples above and updates with PULSEBALL.addMatch( match ).

## Getting Started

```bash
npm install
npm start
```

## Development server

The dev server runs on port 3000.

## Development

This project uses EditorConfig to standardize text editor configuration.
Visit http://editorconfig.org for details.

This project uses ESLint to detect suspicious code in JavaScript files.
Visit http://eslint.org for details.

### Testing

This project uses [Mocha](http://mochajs.org) and [Chai](http://chaijs.com) for testing server-side.

To execute tests:

## Running unit tests (client-side)

Run `npm test` to execute the unit tests via Mocha
