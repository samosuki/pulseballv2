const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const logger = require('morgan');
const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
	console.log(`${req.method} request for '${req.url}' - ${JSON.stringify(req.body)}`);
	next();
});

app.use(express.static("./public"));

app.use(cors());

const PULSEBALL = require('./pulseball');

PULSEBALL.init();
PULSEBALL.addMatch();

app.listen(3000);

console.log("Express app running on port 3000");

module.exports = app;

