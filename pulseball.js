// get Current Table JSON
const rankingsJSON = require('./currentTable');
// get testMatch JSON
const matchJSON = require('./match');

const PULSEBALL = {

  "rankings" : rankingsJSON,
  "match": matchJSON,

  // init app with current table rankings  
  init( rankings ) {
    // console.log(`init called + ${JSON.stringify(rankingsJSON)}`);
  },

  addMatch( match ) {
    console.log(`addMatch called`);
    
    let venueCountry = matchJSON[0].venue.country;
    let team1Name = matchJSON[0].teams[0].name;
    let team2Name = matchJSON[0].teams[1].name;
    let scores = matchJSON[0].scores;
    let teamScores, team1Rating, team2Rating = {};
    let teams = matchJSON[0].teams;

    team1Rating = this.getRanking( rankingsJSON, teams[0].name );
    team2Rating = this.getRanking( rankingsJSON, teams[1].name );
    console.log(`team1Rating > ${JSON.stringify(team1Rating.pts)}`);
    console.log(`team2Rating > ${JSON.stringify(team2Rating.pts)}`);

    console.log(scores);

    teamScores = this.assignHomeTeamPoints(venueCountry, team1Name, team2Name, team1Rating.pts, team2Rating.pts);

    console.log(`teamScores > ${JSON.stringify(teamScores)}`);
    

    let ratingPoints = Math.round(
                        this.calculateRatingsPts( scores[0],
                                                  scores[1],
                                                  teamScores.team1Pts, 
                                                  teamScores.team2Pts)*100)/100;

    console.log(`ratingPoints > ${JSON.stringify(ratingPoints)}`);

    if(ratingPoints !== undefined && team1Rating !== undefined && team2Rating !== undefined) {
      let finalTeamPoints = this.updateTeamPoints(ratingPoints, team1Rating, team2Rating, scores[0], scores[1]);

      console.log(`finalTeamPoints > ${JSON.stringify(finalTeamPoints)}`);
      }

  },

  /**
   * Get Ranking of Team
   *
   * @param {object} rankings initial rankings of all teams
   * @param {string} team Name of Team
   * @return {object} teamRating (Rating) Points of Team
   */
  getRanking( rankings, team ) {
    // find team from by team name
    function findPts(rank) { 
      return rank.team.name === team;
    }
    // console.log(`team ==> ${JSON.stringify(rankings.find(findPts))}`);
    // find an object in an array by one of its properties
    return rankings.find(findPts);
  },

  updateTeamPoints( ratingsPoints, team1Rating, team2Rating, team1score, team2score ) {

    // team1 wins
    if(team1score > team2score) {
      team1Rating.pts += ratingsPoints;
      team2Rating.pts -= ratingsPoints;
    }
    // team2 wins
    else if(team1score < team2score) {
      team1Rating.pts -= ratingsPoints;
      team2Rating.pts += ratingsPoints;
    } else {
      team1Rating.pts += ratingsPoints;
      team2Rating.pts += ratingsPoints;
    }
    
    return { "team1Rating": team1Rating.pts, "team2Rating": team2Rating.pts };
  },

  /**
   * Assign extra points to team playing at home.
   *
   * @param {string} venueCountry Name of Country game is being played in
   * @param {string} teamName1 Name of 1st Team
   * @param {string} teamName2 Name of 2nd Team
   * @param {number} team1Pts Points of 1st Team
   * @param {number} team2Pts Points of 2nd Team
   * @return {object} teamPoints Points of both teams
   */
  assignHomeTeamPoints( venueCountry, teamName1, teamName2, team1Pts, team2Pts ) {

    //console.log(`teamName1 > ${JSON.stringify(teamName1)}`);
    //console.log(`teamName2 > ${JSON.stringify(teamName2)}`);

    // if venue country is the same as the team1 (i.e. team1 playing at home)
    if (venueCountry === teamName1) {
      team1Pts += 3;
    } 
    // if venue country is the same as the team2 (i.e. team2 playing at home)
    if(venueCountry === teamName2) {
      team2Pts += 3;
    }
    // return with no changes if teams are playing at neutral grounds
    return { "team1Pts" : team1Pts, "team2Pts" : team2Pts };
  },

  /**
   * Calculate Ratings Points
   *
   * @param {number} Score of Team 1
   * @param {number} Score of Team 2
   * @return {number} Ratings Points
   */
  calculateRatingsPts( team1Score, team2Score, team1Rating, team2Rating ) {
    let ratingPoints;
    let ratingDifference = this.ratingsDiff(team1Rating, team2Rating);

    // console.log(`team1score > ${JSON.stringify(team1Score)}`);
    // console.log(`team2score > ${JSON.stringify(team2Score)}`);

    // console.log(`ratingDifference > ${JSON.stringify(ratingDifference)}`);
    
    // team 1 wins over team 2
    if(team1Score > team2Score) {  
      return ratingPoints = (1-(Math.round((ratingDifference/10)*1000)/1000));
    }
    // team 2 wins over team 1
    if(team1Score < team2Score) {
      return ratingPoints = (1+(Math.round((ratingDifference/10)*1000)/1000));
    }
    // teams draw match
    if(team1Score == team2Score) {
      return ratingPoints = Math.round((ratingDifference/10)*1000)/1000;
    }
  },

  /**
   * Calculate Ratings Difference
   *
   * @param {number} Score of Team 1
   * @param {number} Score of Team 2
   * @return {number} Ratings Difference
   */
  ratingsDiff(team1Pts, team2Pts) {
    let ratingsFinal;

    let ptDifference = team1Pts - team2Pts;

    if(ptDifference > 10) {
      ratingsFinal = 10;
    }
    else if(ptDifference < -10) {
      ratingsFinal = -10;
    } else {
      ratingsFinal = ptDifference;
    }
    return ratingsFinal;
  }
}

module.exports = PULSEBALL;