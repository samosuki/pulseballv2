const chai = require('chai');
const should = chai.should();
const Pulseball = require('../pulseball');

describe('Pulseball', function() {

  context('Validate Init Data', function() {
    it('it should get Current table', function() {
      const currentTable = Pulseball.rankings;
      Pulseball.init(); // start the app
      currentTable.should.have.lengthOf(5); // current table is array length of 5
    });

    it('it should return the team from the rankings table', function() {
      const currentTable = Pulseball.rankings;
      let team = "France";
      let chosenTeam = Pulseball.getRanking( currentTable, team );
      let expectedTeam = {"team":{"name":"France","id":2},"pos":3,"pts":52.95};
      chosenTeam.should.be.deep.equal(expectedTeam);
    });
  });

  context('Ratings Difference ratingsDiff() Validation', function() {
    it('it should return 10 if Score Difference is more than 10', function() {
      const score1 = 32;
      const score2 = 11;
      let ratingsDiff = Pulseball.ratingsDiff(score1, score2);
      ratingsDiff.should.be.equal(10);
    });
    it('it should return -10 if Score Difference is less than -10', function() {
      const score1 = 11;
      const score2 = 32;
      let ratingsDiff = Pulseball.ratingsDiff(score1, score2);
      ratingsDiff.should.be.equal(-10);
    });
    it('it should return Score Difference (Score 1 - Score 2)', function() {
      const score1 = 20;
      const score2 = 15;
      let ratingsDiff = Pulseball.ratingsDiff(score1, score2);
      ratingsDiff.should.be.equal(5);
    });
  });

  context('Ratings Points calculateRatingsPts() Validation', function() {
    it('it should return accurately if team1 wins over team2', function() {
      const score1 = 23;
      const score2 = 19;
      const rating1 = 55.95;
      const rating2 = 52.32;
      let ratingsPoints = Pulseball.calculateRatingsPts(score1, score2, rating1, rating2);
      ratingsPoints.should.be.equal(0.637);
    });
    it('it should return accurately if team2 wins over team1', function() {
      const score1 = 19;
      const score2 = 23;
      const rating1 = 55.95;
      const rating2 = 52.32;
      let ratingsPoints = Pulseball.calculateRatingsPts(score1, score2, rating1, rating2);
      ratingsPoints.should.be.equal(1.363);
    });
    it('it should return accurately if team1 draws with team2', function() {
      const score1 = 20;
      const score2 = 20;
      const rating1 = 55.95;
      const rating2 = 52.32;
      let ratingsPoints = Pulseball.calculateRatingsPts(score1, score2, rating1, rating2);
      ratingsPoints.should.be.equal(0.363);
    });
  });

  context('Ratings Points assignHomeTeamPoints() Validation', function() {
    it('it should return +3 points for team at home local', function() {
      const team1Pts = 52.95;
      const team2Pts = 52.32;
      const team1Name = "France";
      const team2Name = "England";
      const venueCountry = "France";
      let teamPoints = Pulseball.assignHomeTeamPoints(venueCountry, team1Name, team2Name, team1Pts, team2Pts);
      teamPoints.should.be.deep.equal({"team1Pts" : 55.95, "team2Pts" : 52.32});
    });
    it('it should return same points if venue is neutral', function() {
      const team1Pts = 52.95;
      const team2Pts = 52.32;
      const team1Name = "France";
      const team2Name = "England";
      const venueCountry = "Austria";
      let teamPoints = Pulseball.assignHomeTeamPoints(venueCountry, team1Name, team2Name, team1Pts, team2Pts);
      teamPoints.should.be.deep.equal({"team1Pts" : 52.95, "team2Pts" : 52.32});
    });
  });

});

